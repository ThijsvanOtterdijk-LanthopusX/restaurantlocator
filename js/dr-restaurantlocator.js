var map;
function initMap() {
  map = new google.maps.Map(document.getElementById('storeLocatorMap'), {
    //center: new google.maps.LatLng(10,10),
    zoom: 16
  });

var geocoder = new google.maps.Geocoder();
var allMarkers = new Array();
var markerIterator = 0;

restaurants.forEach(function(restaurant) {
    geocoder.geocode({'address': restaurant.adress}, function(results, status) {
    if (status == 'OK') {
        if (restaurant.long !== null && restaurant.lat !== null && restaurant.long !== '' && restaurant.lat !== '') {
            var latLng = new google.maps.LatLng(restaurant.lat, restaurant.long);

            var marker = new google.maps.Marker({
              position: latLng,
              map: map,
              title: restaurant.title,
              storeid: restaurant.storeid,
            });

        } else {
            var marker = new google.maps.Marker({
              position: results[0].geometry.location,
              map: map,
              title: restaurant.title,
              storeid: restaurant.storeid,
            });
        }

      allMarkers[markerIterator] = marker;
      markerIterator++;
      // create popup
      var contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
              '<h5 id="firstHeading" class="firstHeading">' + restaurant.title + '</h5>'+
            '<div id="bodyContent" class="text-center">'+
              '<a href="https://www.google.nl/maps/dir//'+ restaurant.adress +'" target="_blank" class="button">Route: '+ restaurant.adress +
              '</a>' +
            '</div>'+
            '</div>';

        var infowindow = new google.maps.InfoWindow({
          content: contentString
        });
        // add click action to window
        marker.addListener('click', function() {
          infowindow.open(map, marker);
          storeid = '#' + restaurant.storeid;
          $('.store').removeClass('active');
          $(storeid).addClass('active');
          map.setZoom(16);
          map.panTo(marker.position);
        });
    } else {
      console.log('Geocode was not successful for the following reason: ' + status);
    }


    if(restaurants.length == allMarkers.length) {
      var bounds = new google.maps.LatLngBounds();
      for (var i = 0; i < allMarkers.length; i++) {
       bounds.extend(allMarkers[i].getPosition());
      }
      map.setCenter(bounds.getCenter());
      map.fitBounds(bounds);
      if (restaurants.length == 1) {
          map.setZoom(16);
      } else {
          map.setZoom(map.getZoom()-1);
      }
    }


  });
});


function findObjectByKey(array, key, value) {
    for (var i = 0; i < array.length; i++) {
        if (array[i][key] === value) {
            return array[i];
        }
    }
    return null;
}

jQuery(document).ready(function($){

  $('.store header').click(function(){
    $('.store.active').removeClass('active');
    $(this).parent().addClass('active');
    var storeID = $(this).parent().attr('id');
    var curmarker = findObjectByKey(allMarkers, 'storeid', storeID );
    map.setZoom(16);
    map.panTo(curmarker.position);
  });
    var amountOfStores = $('.store').length;
  if (amountOfStores == 1) {
    $('.store').addClass('active');
    setTimeout(function() {
        map.setZoom(16);
      }, 10);
  }
});
}//initMap
