<?php
/*
Plugin Name: DigitaalRestaurant Restaurant Locator
Plugin URI: https://www.digitaalrestaurant.nl/
Description: Toont een overzicht van je restaurants met interactieve kaart.
Version: 4
Author: DigitaalRestaurant.nl
Author URI: https://www.digitaalrestaurant.nl/
Copyright: DigitaalRestaurant
Text Domain: digitaalrestaurant-restaurantlocator
Domain Path: /localization
Plugin API Id: 19
*/


if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
if( ! class_exists('ACF') ){
	require 'updater/bootstrap.php';
 // turn off this plugin if ACF is not activated
 //exit('<b style="font-family:Arial,Verdana;">Je moet ACF hebben voor deze plugin</b>'); // Since we're using ACF fields, exit if ACF doesnt exist
  add_action( 'admin_init', 'drrl_deactivate' );
  add_action( 'admin_notices', 'drrl_admin_notice' );

  function drrl_deactivate() {
      deactivate_plugins( plugin_basename( __FILE__ ) );
  }
    function drrl_admin_notice() {
       echo '<div class="updated"><p><strong>DigitaalRestaurant Restaurant Locator</strong> heeft Advanced Custom Fields nodig. <strong>De plugin werd gedeactiveerd</strong>.</p></div>';
       if ( isset( $_GET['activate'] ) )
            unset( $_GET['activate'] );
  }

}
if( ! class_exists('DRRL') ) :

class DRRL {
	
	/** @var string The plugin version number */
	var $version = '1.0';
	
	/** @var array The plugin settings array */
	var $settings = array();
	
	/** @var array The plugin data array */
	var $data = array();
	
	/** @var array Storage for class instances */
	var $instances = array();
	
	
	/*
	*  __construct
	*
	*  A dummy constructor to ensure drrl is only initialized once
	*
	*  @type	function
	*  @date	09/10/2018 (DD/MM/YYYY)
	*  @since	5.0.0
	*
	*  @param	N/A
	*  @return	N/A
	*/
	
	function __construct() {
		
		/* Do nothing here */
		
	}

		/*
	*  initialize
	*
	*  The real constructor to initialize DRRL
	*
	*  @type	function
	*  @date	09/10/2018 (DD/MM/YYYY)
	*  @since	5.0.0
	*
	*  @param	$post_id (int)
	*  @return	$post_id (int)
	*/
	function initialize() {
		
		// vars
		$version = $this->version;
		$basename = plugin_basename( __FILE__ );
		$path = plugin_dir_path( __FILE__ );
		$url = plugin_dir_url( __FILE__ );
		$slug = dirname($basename);
		
		
		// settings
		$this->settings = array(
			
			// basic
			'name'				=> __('Digitaal Restaurant Restaurant Locator', 'digitaalrestaurant'),
			'version'			=> $version,
						
			// urls
			'file'				=> __FILE__,
			'basename'			=> $basename,
			'path'				=> $path,
			'url'				=> $url,
			'slug'				=> $slug,
			
			// options
			'show_admin'				=> true,
			'show_updates'				=> true,
			'stripslashes'				=> false,
			'local'						=> true,
			'json'						=> true,
			'save_json'					=> '',
			'load_json'					=> array(),
			'default_language'			=> '',
			'current_language'			=> '',
			'capability'				=> 'manage_options',
			'uploader'					=> 'wp',
			'autoload'					=> false,
			'l10n'						=> true,
			'l10n_textdomain'			=> '',
			'google_api_key'			=> '',
			'google_api_client'			=> '',
			'enqueue_google_maps'		=> true,
			'enqueue_select2'			=> true,
			'enqueue_datepicker'		=> true,
			'enqueue_datetimepicker'	=> true,
			'select2_version'			=> 4,
			'row_index_offset'			=> 1,
			'remove_wp_meta_box'		=> true
		);
		
		
		// constants
		$this->define( 'DRRL', 			true );
		$this->define( 'DRRL_VERSION', 	$version );
		$this->define( 'DRRL_PATH', 		$path );
		//$this->define( 'drrl_DEV', 		true );
		
		
		// api
		// include_once( drrl_PATH . 'includes/api/api-helpers.php');
		// drrl_include('includes/api/api-input.php');
		
		// admin
		if( is_admin() ) {
			// do admin specific stuff
		}
		// actions
		add_action('init',	array($this, 'init'), 5);
		add_action('init',	array($this, 'register_post_types'), 5);
	}
	function drrl_get_path( $path = '' ) {
		
		return DRRL_PATH . $path;
		
	}
	

	function drrl_include( $file ) {
		
		$path = $this->drrl_get_path( $file );
			
		if( file_exists($path) ) {
			
			include_once( $path );
		}
		
	}
	
	/*
	*  init
	*
	*  This function will run after all plugins and theme functions have been included
	*
	*  @type	action (init)
	*  @date	09/10/2018 (DD/MM/YYYY)
	*  @since	5.0.0
	*
	*  @param	N/A
	*  @return	N/A
	*/
	
	function init() {
		
		// bail early if too early
		// ensures all plugins have a chance to add fields, etc
		if( !did_action('plugins_loaded') ) return;
		
		// load files needed for init
		// $this->drrl_include('includes/urmomma.php');
		$this->drrl_include('includes/drrl-schortcode.php');
		$this->drrl_include('includes/acf-fields.php');

	}
	
	/*
	*  register_post_types
	*
	*  This function will register post types and statuses
	*
	*  @type	function
	*  @date	09/10/2018 (DD/MM/YYYY) (DD/MM/YYYY)
	*  @since	1.0
	*
	*  @param	n/a
	*  @return	n/a
	*/
	
	function register_post_types() {
		
		// vars
		// $cap = get_setting('capability');
		register_post_type( 'drrl-items',
			array(
		      'labels' => array(
		        'name' => __( 'Restaurant Locaties'),
		        'singular_name' => __( 'drrl-restaurant_locatie'),
		        'add_new'				=> __( 'Restaurant toevoegen'),
			    'add_new_item'			=> __( 'Restaurant toevoegen'),
			    'edit_item'				=> __( 'Restaurant bewerken'),
			    'new_item'				=> __( 'Nieuw Restaurant'),
			    'view_item'				=> __( 'Restaurant Bekijken'),
			    'search_items'			=> __( 'Restaurants doorzoeken'),
			    'not_found'				=> __( 'Geen restaurants gevonden'),
			    'not_found_in_trash'	=> __( 'Geen restaurants gevonden in de prullenbak'),
				'all_items'				=> __( 'Alle restaurants' ),
				'archives'				=> __( 'Restaurant Archieven' ),
				'attributes'			=> __( 'Restaurant eigenschappen' ),
				'insert_into_item'		=> __( 'Invoegen in restaurant beschrijving' ),
				'uploaded_to_this_item'	=> __( 'Ge-upload naar dit restaurant' )
		      ),
		      'public' => true,
		      'menu_icon' => 'dashicons-location',
		      'has_archive' => true,
		      'hierarchical' => false,
		      'rewrite' => array('slug' => 'restaurant-locaties'),
			  'supports' => array(
					'title',
					'editor',
					'thumbnail',
			   ),
		    )
		);
	}
	
	/*
	*  define
	*
	*  This function will safely define a constant
	*
	*  @type	function
	*  @date	09/10/2018 (DD/MM/YYYY)
	*  @since	5.5.13
	*
	*  @param	$name (string)
	*  @param	$value (mixed)
	*  @return	n/a
	*/
	
	function define( $name, $value = true ) {
		
		if( !defined($name) ) {
			define( $name, $value );
		}
		
	}
	
	/**
	*  has_setting
	*
	*  Returns true if has setting.
	*
	*  @date	09/10/2018 (DD/MM/YYYY)
	*  @since	5.6.5
	*
	*  @param	string $name
	*  @return	boolean
	*/
	
	function has_setting( $name ) {
		return isset($this->settings[ $name ]);
	}
	
	/**
	*  get_setting
	*
	*  Returns a setting.
	*
	*  @date	09/10/2018 (DD/MM/YYYY)
	*  @since	5.0.0
	*
	*  @param	string $name
	*  @return	mixed
	*/
	
	function get_setting( $name ) {
		return isset($this->settings[ $name ]) ? $this->settings[ $name ] : null;
	}
	
	/**
	*  update_setting
	*
	*  Updates a setting.
	*
	*  @date	09/10/2018 (DD/MM/YYYY)
	*  @since	5.0.0
	*
	*  @param	string $name
	*  @param	mixed $value
	*  @return	n/a
	*/
	
	function update_setting( $name, $value ) {
		$this->settings[ $name ] = $value;
		return true;
	}
	
	/**
	*  get_data
	*
	*  Returns data.
	*
	*  @date	09/10/2018 (DD/MM/YYYY)
	*  @since	5.0.0
	*
	*  @param	string $name
	*  @return	mixed
	*/
	
	function get_data( $name ) {
		return isset($this->data[ $name ]) ? $this->data[ $name ] : null;
	}
	
	
	/**
	*  set_data
	*
	*  Sets data.
	*
	*  @date	09/10/2018 (DD/MM/YYYY)
	*  @since	5.0.0
	*
	*  @param	string $name
	*  @param	mixed $value
	*  @return	n/a
	*/
	
	function set_data( $name, $value ) {
		$this->data[ $name ] = $value;
	}
	
	
	/**
	*  get_instance
	*
	*  Returns an instance.
	*
	*  @date	09/10/2018 (DD/MM/YYYY)
	*  @since	5.6.9
	*
	*  @param	string $class The instance class name.
	*  @return	object
	*/
	
	function get_instance( $class ) {
		$name = strtolower($class);
		return isset($this->instances[ $name ]) ? $this->instances[ $name ] : null;
	}
	
	/**
	*  new_instance
	*
	*  Creates and stores an instance.
	*
	*  @date	09/10/2018 (DD/MM/YYYY)
	*  @since	5.6.9
	*
	*  @param	string $class The instance class name.
	*  @return	object
	*/
	
	function new_instance( $class ) {
		$instance = new $class();
		$name = strtolower($class);
		$this->instances[ $name ] = $instance;
		return $instance;
	}
	
}
	/**
	*  drrl_enqueue_assets
	*
	*  Enqueues the scripts and styles needed for the shortcode to work.
	*
	*  @date	09/10/2018 (DD/MM/YYYY)
	*  @since	5.0.0
	*
	*  @param	string $name
	*  @return	mixed
	*/
	function drrl_enqueue_assets() {
		// the actual script
		wp_register_script("drrl-script", plugins_url("js/dr-restaurantlocator.js", __FILE__), array(), "1.0", false);
		// google Maps JS API: Needs JS API + GEOCODER
		wp_register_script("drrl-googlemaps", "https://maps.googleapis.com/maps/api/js?key=AIzaSyBhxz2nbr4XAYrSDdZy0cD447DIubCJm1c&callback=initMap", array(), "1.0", false);
		// styles
		wp_register_style("drrl-style", plugins_url("sass.php/drrl-style.scss", __FILE__), array(), "1.0", "all");
	}
	add_action( 'init', 'drrl_enqueue_assets' );
/*
*  drrl
*
*  The main function responsible for returning the one true drrl Instance to functions everywhere.
*  Use this function like you would a global variable, except without needing to declare the global.
*
*  Example: <?php $drrl = drrl(); ?>
*
*  @type	function
*  @date	09/10/2018 (DD/MM/YYYY)
*  @since	4.3.0
*
*  @param	N/A
*  @return	(object)
*/

function drrl() {
	
	// globals
	global $drrl;
	
	// initialize
	if( !isset($drrl) ) {
		$drrl = new drrl();
		$drrl->initialize();
	}
	
	// return
	return $drrl;
	
}

// initialize
drrl();

endif; // class_exists check

?>
