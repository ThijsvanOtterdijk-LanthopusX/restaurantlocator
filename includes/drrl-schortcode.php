<?php add_shortcode('dr_restaurant_locaties', 'drrl_display_shortcode');
  function drrl_display_shortcode() {
  //load assets first
  wp_enqueue_script("drrl-script");
  wp_enqueue_style("drrl-style");
  wp_enqueue_script("drrl-googlemaps");

	//args
	$args = array('post_type' => 'drrl-items');
	// The Query
	$drrlQuery = new WP_Query($args);
	// The Loop
	if ($drrlQuery->have_posts() ) {
		$restaurantsArray = array();
		$html = '<div class="storeLocator">
          <div id="storeLocatorMap"></div>';
		while ($drrlQuery->have_posts() ) { $drrlQuery->the_post();

			$html .= '
          <section class="store row" id="'. get_the_ID() .'">
            <header class="col-12">
              <h3>'. get_the_title() .'</h3>
            </header>
            <div class="content">
              <div class="col-12 col-md-6 restaurantGegevens">
                <ul>
                  <li>'. get_field('straatnaam') .' '. get_field('huisnummer') .'</li>
                  <li>'. get_field('postcode') .' '. get_field('plaatsnaam') .'</li>
                  <li>'. get_field('land') .'</li>
                  <li><a href="tel:'. get_field('telefoonnummer') .'">'. get_field('telefoonnummer') .'</a></li>
                  <li><a href="tel:'. get_field('e-mail_adres') .'">'. get_field('e-mail_adres') .'</a></li>
                </ul>
              <a href="https://www.google.com/maps/dir//'. get_field('straatnaam') .'+'. get_field('huisnummer') .','. get_field('plaatsnaam') .'" class="button routeButton" target="_blank">'. __( 'Bekijk de Route', 'digitaalrestaurant' ) .'</a>';
              if (get_field('bestellen_link')) {   $html  .= '<a href="'. get_field('bestellen_link') .'" class="button bestellenButton">'. __( 'Bestellen', 'digitaalrestaurant' ) .'</a>';}
              if (get_field('reserveren_link')) {   $html .= '<a href="'. get_field('reserveren_link') .'" class="button reserverenButton">'. __( 'Reserveren', 'digitaalrestaurant' ) .'</a> ';}
            $html .= '</div>
              <div class="col-12 col-md-6">
                <b>'. __( 'Openingstijden', 'digitaalrestaurant' ) .'</b>
                <table>
                  <tr>
                    <td>'. __('Maandag','digitaalrestaurant') .':</td>
                    <td>'. get_field('maandag') .'</td>
                  </tr>
                  <tr>
                    <td>'. __('Dinsdag','digitaalrestaurant') .':</td>
                    <td>'. get_field('dinsdag') .'</td>
                  </tr>
                  <tr>
                    <td>'. __('Woensdag','digitaalrestaurant') .':</td>
                    <td>'. get_field('woensdag') .'</td>
                  </tr>
                  <tr>
                    <td>'. __('Donderdag','digitaalrestaurant') .':</td>
                    <td>'. get_field('donderdag') .'</td>
                  </tr>
                  <tr>
                    <td>'. __('Vrijdag','digitaalrestaurant') .':</td>
                    <td>'. get_field('vrijdag') .'</td>
                  </tr>
                  <tr>
                    <td>'. __('Zaterdag','digitaalrestaurant') .':</td>
                    <td>'. get_field('zaterdag') .'</td>
                  </tr>
                  <tr>
                    <td>'. __('Zondag','digitaalrestaurant') .':</td>
                    <td>'. get_field('zondag') .'</td>
                  </tr>
                </table>';
                if (get_field('foto')) {
                  $html .=  '</div><div class="col-12 col-md-6" style="padding-top:15px;">
                  '. get_field('restaurant_informatie') .'
                </div>
                <div class="col-12 col-md-6" style="padding-top:15px;">
                <img src="'. get_field('foto') .'" alt="foto van ons restaurant in '. get_field('plaatsnaam') .'" /></div>';
                } else {
                   $html .=  '</div><div class="col-12" style="padding-top:15px;">
                  '. get_field('restaurant_informatie') .'
                </div>';
                }
                $html .= '</div>
                </section>';

          $restaurantsArray[] = array(
          	'storeid'	=> strval( get_the_ID()),
          	'title' 	=> get_the_title(),
          	'adress' 	=> get_field('straatnaam') .' '. get_field('huisnummer').' '. get_field('plaatsnaam'),
            'long'      => get_field('longitude'),
            'lat'       => get_field('latitude')
          	);
		} //endwhile
		$finalArray = json_encode($restaurantsArray);
		$html .= '</div>';
		$html .= '<script>
          var restaurants = ' . $finalArray .';
        </script>';
    /*$html .= "        <script>
          var restaurants = [
            {storeid:'42', title:'GourmetBar Charleroi', adress:'Place Verte 17 Charleroi'},
            {storeid:'43', title:'GourmetBar Gent', adress:'Goudenleeuwplein 5 Gent'}
          ];
        </script>";*/
	}//endif have_posts
	wp_reset_postdata();

    return $html;
}
?>
