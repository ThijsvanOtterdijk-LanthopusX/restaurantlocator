<?php
namespace LanX\Updater;

class Core
{
    public static function init()
    {
        $themeUpdater = new \LanX\Updater\ThemeUpdater(
            \LanX\Updater\Model\ThemeCollection::all()
        );
        $pluginUpdater = new \LanX\Updater\PluginUpdater(
            \LanX\Updater\Model\PluginCollection::all()
        );
        $themeUpdater->poll();
        $pluginUpdater->poll();
    }
}
