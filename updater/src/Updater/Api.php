<?php
namespace LanX\Updater;

class Api
{
    const STATUS_URL = 'https://repo.lanthopusx.nl/api/thema';

    const PACKAGE_URL = 'https://repo.lanthopusx.nl/api/thema/package/%d/%s/latest.zip';

    const LICENSE_URL = 'https://repo.lanthopusx.nl/api/licentie/%s/can_update/thema/%s/domain/%s';

    public static function getPackageUrl($packageId, $licenseString, $themeName, $themeVersion)
    {
        return sprintf(self::PACKAGE_URL, $packageId, $licenseString);
    }

    public static function getLicenseUrl($license, $themeId)
    {
        return sprintf(self::LICENSE_URL, $license, $themeId, wp_parse_url(get_site_url(), 1));
    }
}
