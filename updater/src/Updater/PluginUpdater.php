<?php
namespace LanX\Updater;

use LanX\Updater\Admin\Settings;

class PluginUpdater
{
    private $plugins;

    public function __construct(
        array $plugins
    )
    {
        $this->plugins = $plugins;
    }

    public function poll()
    {
        foreach ($this->plugins as $plugin) {
            try {
                if ($plugin->isValidLicense(Settings::getLicense())) {
                    add_filter('pre_set_site_transient_update_plugins', [$plugin, 'getTransient']);
                } else {
                    add_action('admin_notices', [\LanX\Updater\Admin\License\InvalidNotice::class, 'display']);
                }
            } catch (\Exception $e) {
                $notice = new \LanX\Updater\Admin\ExceptionNotice($e);
                add_action('admin_notices', [$notice, 'display']);
            }
        }
    }
}
