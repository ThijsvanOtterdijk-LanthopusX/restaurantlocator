<?php
namespace LanX\Updater\Model;

use LanX\Updater\Model\Theme;

class PluginCollection
{
    public static function all()
    {
        $plugins = [];
        $allPlugins = get_plugins();

        foreach ($allPlugins as $key => $plugin) {
            $tmpPlugin = new Plugin($key, $plugin);

            if ($tmpPlugin->getId() !== "") {
                $plugins[] = $tmpPlugin;
            }
        }
        return $plugins;
    }
}
