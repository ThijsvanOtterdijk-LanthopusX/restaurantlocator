<?php
namespace LanX\Updater\Model;

use LanX\Updater\Model\Theme;

class ThemeCollection
{
    public static function all()
    {
        $themes = [];
        $allThemes = wp_get_themes();

        foreach ($allThemes as $theme) {
            $tmpTheme = new Theme($theme);

            if ($tmpTheme->getId() !== "") {
                $themes[] = $tmpTheme;
            }
        }
        return $themes;
    }
}
