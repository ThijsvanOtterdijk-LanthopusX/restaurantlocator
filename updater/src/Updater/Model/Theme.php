<?php
namespace LanX\Updater\Model;

use LanX\Updater\Admin\Settings;
use LanX\Updater\Api;
use LanX\Updater\Exception\CurlException;

class Theme
{
    private $theme;

    public function __construct(\WP_Theme $theme)
    {
        $this->theme = $theme;
    }

    public function getId()
    {
        return get_file_data($this->theme->get_stylesheet_directory() . '/style.css', ['Theme API Id'])[0];
    }

    public function getVersion()
    {
        return $this->theme->get('Version');
    }

    public function isValidLicense($license)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, Api::getLicenseUrl($license, $this->getId()));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if (defined('WP_DEBUG') && WP_DEBUG === true) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        }

        $response = json_decode(curl_exec($ch));

        if (curl_errno($ch)) {
            throw new CurlException(curl_error($ch));
        }
        if ($response === null) {
            throw new CurlException('Null response from: ' . Api::getLicenseUrl($license, $this->getId()));
        }

        curl_close($ch);
        return $response->can_update;
    }

    public function getRemoteStatus()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, Api::STATUS_URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, [
            'theme' => $this->getId(),
            'license' => Settings::getLicense()
        ]);

        if (defined('WP_DEBUG') && WP_DEBUG === true) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        }

        $response = json_decode(curl_exec($ch));
        if (curl_errno($ch)) {
            throw new CurlException(curl_error($ch));
        }

        curl_close($ch);

        return $response;
    }

    public function shouldUpdate()
    {
        $remoteStatus = $this->getRemoteStatus();
        if ($remoteStatus === null || !is_object($remoteStatus)) {
            return false;
        }
        if (version_compare($this->getVersion(), $remoteStatus->version, '<')) {
            return true;
        }
        return false;
    }

    public function getTransient($transient)
    {
        if ($this->shouldUpdate()) {
            $slug = $this->theme->get_stylesheet();

            $transient->response[$slug] = [
                'slug' => $slug,
                'new_version' => $this->getRemoteStatus()->version,
                'package' => Api::getPackageUrl($this->getId(), Settings::getLicense(), $slug, $this->getRemoteStatus()->version),
                'url' => Api::getPackageUrl($this->getId(), Settings::getLicense(), $slug, $this->getRemoteStatus()->version)
            ];
        }

        return $transient;
    }

    public function setLocalVersion()
    {
        $stylesheet = $this->theme->get_stylesheet_directory() . '/style.css';


        $fh = fopen($stylesheet, 'r+');
        $contents = fread($fh, filesize($stylesheet));
        $contents = str_replace('\r', '\n', $contents);

        if ( preg_match( '/^[ \t\/*#@]*' . preg_quote( 'Version', '/' ) . ':(.*)$/mi', $contents, $match ) && $match[1] ) {
            $content = str_replace($match[0], "Version: {$this->getRemoteStatus()->version}\r\n", $contents);

            $fha = fopen($stylesheet, 'w');
            fwrite($fha, $content);
            fclose($fha);
        }
        fclose($fh);
    }
}
