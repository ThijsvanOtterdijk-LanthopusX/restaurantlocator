<?php

namespace LanX\Updater\Admin\License;

class InvalidNotice
{
    public static function display()
    {
        ob_start();
        include ABS_UPDATER_PATH . '/templates/invalid-notice.phtml';
        $content = ob_get_clean();

        echo $content;
    }
}
