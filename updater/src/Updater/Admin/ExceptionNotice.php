<?php
namespace LanX\Updater\Admin;

class ExceptionNotice
{
    private $exception;

    public function __construct(\Exception $exception)
    {
        $this->exception = $exception;
    }

    public function display()
    {
        ob_start();
        if (defined('WP_DEBUG') && WP_DEBUG === true) {
            $message = $this->exception->getMessage();
        } else {
            $message = 'Something went wrong';
        }
        include ABS_UPDATER_PATH . '/templates/exception-notice.phtml';
        $content = ob_get_clean();
        echo $content;
    }
}
