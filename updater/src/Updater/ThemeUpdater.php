<?php
namespace LanX\Updater;

use LanX\Updater\Admin\Settings;

class ThemeUpdater
{
    private $themes;

    public function __construct(
        array $themes
    )
    {
        $this->themes = $themes;
    }

    public function poll()
    {
        foreach ($this->themes as $theme) {
            try {
                if ($theme->isValidLicense(Settings::getLicense())) {
                    add_filter('pre_set_site_transient_update_themes', [$theme, 'getTransient']);
                } else {
                    add_action('admin_notices', [\LanX\Updater\Admin\License\InvalidNotice::class, 'display']);
                }
            } catch (\Exception $e) {
                $notice = new \LanX\Updater\Admin\ExceptionNotice($e);
                add_action('admin_notices', [$notice, 'display']);
            }
        }
    }
}
