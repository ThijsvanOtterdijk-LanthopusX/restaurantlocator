<?php
if (!function_exists('add_action')) {
	echo 'Do not call me directly';
	exit;
}

try {
    require get_template_directory() . '/vendor/autoload.php';
} catch (\Exception $e) {
    echo $e->getMessage();
}

define('ABS_UPDATER_PATH', get_template_directory() . '/updater');
/**
*   Theme polling
*/
add_action('admin_init', [\LanX\Updater\Core::class, 'init']);

/**
*   Settings
*/
add_action('init', [\LanX\Updater\Admin\Settings::class, 'addOptions'], 99);
