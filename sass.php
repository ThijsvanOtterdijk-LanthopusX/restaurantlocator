<?php define('WP_USE_THEMES', false);
require_once( explode( "wp-content" , __FILE__ )[0] . "wp-load.php" );
require_once realpath(dirname(__FILE__))."/css/sasscompiler/scss.inc.php";
use Leafo\ScssPhp\Compiler;
use Leafo\ScssPhp\Server;
$scss = new Compiler();
$directory = "css";
$cache_dir = "css/scss_cache";
$scss->setFormatter('Leafo\ScssPhp\Formatter\Compressed');
$scss->addImportPath(function($path) {
    if (!file_exists('css/'.$path)) return null;
    return 'css/'.$path;
});

// $primaryColor: #000; 	/* used for backgrounds and such.*/
// $secondaryColor:#fff; 	/* used for font colors on backgrounds and such.*/
// $accentColor: red;	 	/* Used for links, hovers (and such)*/
// $baseFontSize: 18px;
// 
// $titleFont:'Open Sans'; /* Font-Family for all title elements (h1-h6)*/
// $titleFontColor:red;	/* Font-color for all title elements (h1-h6)*/
// $contentFont: 'Arial';	/* Font-Family for paragraphs blockquotes etc.*/
// $contentFontColor:#000;	/*Font-color for paragraphs blockquotes etc.*/

// set colors
if (get_field('primarycolor','option')){
	$primaryColor	= get_field('primarycolor','option');
} else {
	$primaryColor	= 'rgb(0,177,235)';
}
if (get_field('secondarycolor','option')){
	$secondaryColor	= get_field('secondarycolor','option');
} else {
	$secondaryColor	= 'rgb(42,42,42)';
}
if (get_field('accent_color','option')){
	$accentColor	= get_field('accent_color','option');
} else {
	$accentColor	= 'rgb(42,42,42)';
}
if (get_field('content_achtergrond','option')){
	$contentBackgroundColor	= get_field('content_achtergrond','option');
} else {
	$contentBackgroundColor	= 'rgba(255,255,255,0.9)';
}
if (get_field('title_color','option')){
	$titleFontColor	= get_field('title_color','option');
} else {
	$titleFontColor	= 'rgb(42,42,42)';
}
if (get_field('contentfont_color','option')){
	$contentFontColor	= get_field('contentfont_color','option');
} else {
	$contentFontColor	= 'rgb(42,42,42)';
}
// set fonts
if (get_field('title_font','option')){
	$titleFont	= 	get_field('primary_font','option');
	$titleFont 	=	$titleFont["font"];
} else {
	$titleFont	= 'Lato, sans-serif';
}

if (get_field('content_font','option')){
	$titleFont	= 	get_field('primary_font','option');
	$titleFont 	=	$titleFont["font"];
} else {
	$titleFont	= 'Lato, sans-serif';
}
//font-size
if (get_field('fontsize','option')){
	$baseFontSize		= get_field('fontsize','option') .'px';
} else {
	$baseFontSize		= '18px';
}
$scss->getVariables();
$scss->setVariables(
	array(
		'$primaryColor' 	=> $primaryColor,	/* used for backgrounds and such.*/
		'$secondaryColor'	=> $secondaryColor,	/* used for font colors on backgrounds and such.*/
		'$accentColor' 		=> $accentColor,	/* Used for links, hovers (and such)*/
		'$contentBackgroundColor' => $contentBackgroundColor,
		'$baseFontSize' 	=> $baseFontSize,	
		'$titleFont' 		=> $titleFont,	/* Font-Family for all title elements (h1-h6)*/
		'$titleFontColor' 	=> $titleFontColor,	/* Font-color for all title elements (h1-h6)*/
		'$contentFont' 		=> $contentFont,	/* Font-Family for paragraphs blockquotes etc.*/
		'$contentFontColor' => $contentFontColor	/*Font-color for paragraphs blockquotes etc.*/

	)
);
$server = new Server($directory,$cache_dir,$scss);
$cssfile = $server->serve();
